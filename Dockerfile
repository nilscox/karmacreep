FROM node:latest

RUN mkdir -p /var/www/
WORKDIR /var/www/

EXPOSE 4321

COPY ./package.json /var/www/
RUN npm install
RUN npm install -g mocha

COPY ./ /var/www/

RUN npm run webpack

ENTRYPOINT []
CMD [ "./start.sh" ]
