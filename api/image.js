const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const config = require('../config');
const db = config.db;

const sequelize = new Sequelize(db.dbname, db.user, db.password, {
  host: db.host,
  dialect: 'postgres',
  logging: console.log.bind(console),
});

const Image = sequelize.define('image', {

  filename: {
    type: Sequelize.STRING,
    unique: true,
  },

  sort: {
    type: Sequelize.INTEGER,
  }

}, {

  getterMethods: {
    hd: function () {
      return '/' + path.join('images', 'hd', this.filename);
    },
    web: function () {
      return '/' + path.join('images', 'web', this.filename);
    },
    thumb: function () {
      return '/' + path.join('images', 'thumb', this.filename);
    },
  }

});

const readfile = (filename) => {
  const filepath = path.resolve(__dirname, filename);
  return new Promise((resolve, reject) => fs.readFile(filepath, (err, data) => {
    if (err)
      reject(err);
    else
      resolve(data.toString());
  }));
};

const query = (sql) => {
  return sequelize.query(sql);
};

const sync = () => {
  return sequelize.sync()
    .then(() => readfile('../sql/create_trigger.sql'))
    .then(sql => query(sql))
    .catch(err => {
      console.error(err);
      process.exit(1);
    });
};

module.exports = {
  Image,
  sync,
  query,
};
