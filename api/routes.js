const fs = require('fs');
const path = require('path');
const express = require('express');
const multer = require('multer');
const sharp = require('sharp');

const Image = require('./image').Image;

const config = require('../config');

const IMAGES_DIR = path.resolve(__dirname, '..', 'public', 'images');

const storage = multer.diskStorage({

  destination: function (req, file, cb) {
    cb(null, path.join(IMAGES_DIR, 'hd'))
  },

  filename: function (req, file, cb) {
    let filename = file.originalname;

    while (fs.existsSync(path.join(IMAGES_DIR, 'hd', filename)))
    {
      let match = /^(.*)\.([^.]+)$/.exec(filename);

      if (!match)
        return cb('Invalid file name');

      filename = match[1];
      const ext = match[2];

      match = /^(.*)-(\d+)$/.exec(filename);
      if (match)
      {
        const n = parseInt(match[2]);
        filename = match[1] + '-' + (n + 1);
      }
      else {
        filename += '-1';
      }

      filename += '.' + ext;
    }

    cb(null, filename)
  }
});

const upload = multer({
  storage: storage,

  fileFilter: (req, file, cb) => {
    cb(null, file.mimetype.startsWith('image/'));
  },

});

const saveImage = (filename, filepath, size, dirname) => {
  let image = sharp(filepath);

  if (size)
    image = image.resize(size);

  return image.toFile(path.join(IMAGES_DIR, dirname, filename))
};

const handleError = (err, res) => {
  if (config.app.dev)
    console.error(err);
  res.status(500).end();
};

const router = express.Router();

router.post('/', upload.single('image'), (req, res) => {

  if (!req.file)
    return res.status(400).end();

  const filename = req.file.filename;
  const filepath = req.file.path;

  saveImage(filename, filepath, 600, 'web')
    .then(() => saveImage(filename, filepath, 120, 'thumb'))
    .then(() => Image.max('sort'))
    .then(sort => Image.create({
      filename: filename,
      sort: sort + 1 || 0,
    }))
    .then(image => {
      if (!image)
        return res.status(500).end();
      res.json(image.get());
    })
    .catch(err => handleError(err, res));
});

router.param('id', (req, res, next, id) => {
  Image
    .findByPrimary(parseInt(id))
    .then(image => {
      if (!image)
        res.status(404).end();
      else {
        req.image = image;
        next();
      }
    })
    .catch(err => handleError(err, res));
});

router.delete('/:id(\\d+)', (req, res) => {
  req.image.destroy()
    .then(() => res.end())
    .catch(err => handleError(err, res));
});

router.post('/:id(\\d+)/sort', (req, res) => {
  if (!req.body)
    return res.status(400).end();

  const sort = req.body.sort;

  if (typeof sort != 'number')
    return res.status(400).end();

  req.image.update({ sort })
    .then(image => image.get({ plain: true }))
    .then(image => res.json(image))
    .catch(err => handleError(err, res));
});

module.exports = router;
