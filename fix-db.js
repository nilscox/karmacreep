const { Image, sync, query } = require('./api/image');

let images;

sync()
  .then(() => Image.all({ order: ['sort'] }))
  .then(imgs => {
    images = imgs.map((img, n) => {
      img.sort = n;
      return img.get();
    });
    return Image.truncate();
  })
  .then(() => Image.bulkCreate(images))
  .then(() => query('SELECT setval(\'images_id_seq\', (SELECT MAX(id) FROM images))'))
  .then(() => {
    console.log('done.');
    process.exit(0);
  });
