const auth = require('http-auth');
const bodyParser = require('body-parser');
const express = require('express');
const morgan = require('morgan');
const path = require('path');

const users = require('./auth');
const config = require('./config');
const image = require('./api/image');

const app = express();

const basic = auth.basic({
    realm: 'Web.',
    skipUser: true,
  }, (username, password, cb) => cb(users[username] === password)
);

app.use(morgan(config.app.dev ? 'dev' : 'combined'));
app.use(bodyParser.json());

app.use(express.static(path.resolve(__dirname, 'public'), { maxAge: 86400000 }));
app.use('/admin/:page?', auth.connect(basic), express.static(path.resolve(__dirname, 'admin')));

app.get('/api/image', (req, res) => {
  image.Image
    .all({
      order: [
        [ 'sort', 'DESC' ] ,
      ],
    })
    .then(images => images.map(image => image.get({ plain: true })))
    .then(images => res.json(images))
    .catch(err => {
      if (config.app.dev)
        console.error(err);
      res.status(500).end();
    });
});

app.use('/api/image', auth.connect(basic), require('./api/routes'));

app.use((req, res) => {
  res.status(404)
    .sendFile(path.resolve(__dirname, 'public', '404.html'));
});

app.use((err, req, res, next) => {
  if (config.app.dev)
    console.log(err);
  res.status(500).end();
});

image.sync().then(() => {
  const port = process.env.PORT || config.app.port || 4242;
  console.log('App started on port ' + port);
  app.listen(port);
});
