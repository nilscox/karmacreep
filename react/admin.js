import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory, Router, Route, Link } from 'react-router';
import request from 'superagent';
import FileInput from 'react-file-input';
import InputRange from 'react-input-range';
import DragSortableList from './react-drag-sortable';

const ImagesList = ({ page, pageSize, totalPages, pageSizes, setPageSize, arrows, images, onSort, onRemove }) => {

  const imgs = images.map(image => ({
    content: <img
      data-id={image.id}
      onDoubleClick={() => onRemove(image.id)}
      src={image.thumb} />,
  }));

  const arrowPrev = arrows.prev ? (
    <div className="page-arrow page-arrow-prev">
      <Link to={'/admin/' + (page - 1)}>
        <span className="glyphicon glyphicon-chevron-left" />
      </Link>
    </div>
  ) : undefined;

  const arrowNext = arrows.next ? (
    <div className="page-arrow page-arrow-next">
      <Link to={'/admin/' + (page + 1)}>
        <span className="glyphicon glyphicon-chevron-right" />
      </Link>
    </div>
  ) : undefined;

  const pageSelect = page && totalPages > 1 ? (
    <InputRange
      minValue={1}
      maxValue={totalPages}
      value={page}
      onChange={value => browserHistory.push('/admin/' + value)} />
  ) : undefined;

  const pageSizeSelect = (
    <select className="form-control" onChange={e => setPageSize(parseInt(e.target.value))} defaultValue={pageSize}>
      {pageSizes.map(n => <option key={n} value={n}>{n} images</option>)}
    </select>
  );

  return (
    <div id="images">

      <h1>Images</h1>

      <div id="controls">
        <div id="page-size-select">
          {pageSizeSelect}
        </div>
        <div id="page-select" className="row">
          <div className="col-xs-1">{arrowPrev}</div>
          <div className="col-xs-10">{pageSelect}</div>
          <div className="col-xs-1">{arrowNext}</div>
        </div>
        <div className="clearfix"></div>
      </div>

      <DragSortableList
        items={imgs}
        onSort={onSort}
        type="grid"
        placeholder={<div className="placeholder"></div>}
        dropBackTransitionDuration={0.3}
        moveTransitionDuration={0.3}
      />

    </div>
  );
};

const UploadForm = ({ addImage }) => {
  let image;
  return (
    <div id="upload">

      <h1>Upload</h1>

      <form id="upload" onSubmit={e => {
        e.preventDefault();
        addImage(image);
      }}>

        <div className="input-group">
          <FileInput
            type="file"
            name="image"
            className="form-control"
            onChange={e => image = e.target.files[0]} />
          <span className="input-group-btn"><input type="submit" className="btn btn-default" value="Envoyer" /></span>
        </div>

      </form>
    </div>
  );
};

class App extends React.Component {

  constructor(props) {
    super(props);

    this.totalPages = null;
    this.page = null;

    this.state = {
      images: [],
      freeze: true,
      pageSize: 20,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    const totalPages = parseInt(this.state.images.length / this.state.pageSize);

    if (totalPages !== this.state.totalPages)
      this.setState({ totalPages });

    if (!this.state.page || prevProps.params.page !== this.props.params.page) {
      const page = parseInt(this.props.params.page);
      if (!page || page <= 0 || page > totalPages)
        browserHistory.push('/admin/1');
      else
        this.setState({ page });
    }
  }

  componentDidMount() {
    this.fetchImages();
  }

  getPage(list) {
    const page = parseInt(this.props.params.page);
    return list.slice(this.state.pageSize * (page - 1), this.state.pageSize * page);
  }

  fetchImages() {
    request.get('/api/image')
      .then(res => {
        this.old = res.body.map(img => img.id);
        this.setState({ freeze: false, images: res.body });
      })
      .catch(err => console.error(err));
  }

  addImage(image) {
    if (this.state.freeze || !image)
      return;

    if (!image.type.startsWith('image/'))
      return;

    this.setState({ freeze: true });

    request.post('/api/image')
      .attach('image', image)
      .then(res => {
        this.setState({
          images: [ res.body, ...this.state.images ],
          freeze: false,
        });
      })
      .catch(err => console.log(err));
  }

  sortImages(list) {
    const ids = list.map(item => item.content.props['data-id']);
    const old = this.getPage(this.old);

    let sort = null;

    let id = ids.find((item, i) => {
      sort = i;
      return item !== old[sort];
    });

    if (!id)
      return;

    if (ids[sort] === old[sort + 1]) {
      id = old[sort];
      sort = ids.indexOf(id);
    }

    ++sort;

    this.setState({ freeze: true });

    request.post('/api/image/' + id + '/sort', {})
      .send({ sort: this.old.length - sort - this.state.pageSize * (this.state.page - 1) })
      .then(() => this.fetchImages())
      .catch(err => console.error(err));
  }

  removeImage(id) {
    if (this.state.freeze)
      return;

    if (window.confirm('Supprimer l\'image ?')) {
      this.setState({ freeze: true });
      request.delete('/api/image/' + id)
        .then(() => this.fetchImages())
        .catch(err => console.error(err));
    }
  }

  render() {
    if (this.state.freeze)
      return <div id="content"><h1>Loading...</h1></div>;

    const { page, totalPages, pageSize } = this.state;

    const setPageSize = size => {
      this.setState({ pageSize: size, page: 1 });
      browserHistory.push('/admin/1');
    };

    return (
      <div id="content">
        <UploadForm addImage={this.addImage.bind(this)} />
        <ImagesList
          page={page}
          pageSize={pageSize}
          pageSizes={[5, 10, 20, 50, 100]}
          setPageSize={setPageSize}
          totalPages={totalPages}
          arrows={{prev: page > 1, next: page < totalPages}}
          images={this.getPage(this.state.images)}
          onSort={this.sortImages.bind(this)}
          onRemove={this.removeImage.bind(this)} />
      </div>
    );
  }

}

ReactDOM.render(
  <Router history={browserHistory}>
    <Route path="/admin/(:page)" component={App} />
  </Router>,
  document.getElementById('wrapper')
);
