import React from 'react';
import ReactDOM from 'react-dom';
import Lightbox from 'react-image-lightbox';
import LazyLoad from 'react-lazy-load';
import request from 'superagent';

let ImagesList = React.createClass({

  getInitialState() {
    return {
      index: 0,
      isOpen: false,
      images: []
    };
  },

  componentWillMount() {
    request.get('/api/image')
      .then(res => this.setState({images: res.body}))
      .catch(console.error.bind(console));
  },

  openLightbox(index) {
    this.setState({isOpen: true, index: index % this.state.images.length});
  },

  closeLightbox() {
    this.setState({isOpen: false});
  },

  moveNext() {
    this.setState({index: (this.state.index + 1) % this.state.images.length});
  },

  movePrev() {
    this.setState({index: (this.state.index + this.state.images.length - 1) % this.state.images.length});
  },

  render() {
    let images = this.state.images.map((image, i) => (
      <LazyLoad key={i} height={200}>
        <img src={image.web} onClick={() => this.openLightbox(i)} />
      </LazyLoad>
    ));

    let lightbox = '';
    if (this.state.isOpen) {
      let imgs = this.state.images;
      lightbox = (
        <Lightbox
          mainSrc={imgs[this.state.index].hd}
          nextSrc={imgs[(this.state.index + 1) % imgs.length].hd}
          prevSrc={imgs[(this.state.index + imgs.length - 1) % imgs.length].hd}
          onCloseRequest={this.closeLightbox}
          onMovePrevRequest={this.movePrev}
          onMoveNextRequest={this.moveNext}
        />
      );
    }

    return (
      <div id="images">
        {images}
        {lightbox}
      </div>
    );
  }
});

ReactDOM.render(
  <ImagesList />,
  document.getElementById('content')
);