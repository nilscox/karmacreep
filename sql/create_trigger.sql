CREATE OR REPLACE FUNCTION image_after_update()
RETURNS TRIGGER AS $$
BEGIN
    IF OLD.sort < NEW.sort THEN
        UPDATE images SET sort = sort - 1 WHERE id <> OLD.id AND sort BETWEEN OLD.sort AND NEW.sort;
    ELSE
        UPDATE images SET sort = sort + 1 WHERE id <> OLD.id AND sort BETWEEN NEW.sort AND OLD.sort;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS image_after_update_trigger ON images;
CREATE TRIGGER image_after_update_trigger
    AFTER UPDATE ON images
    FOR EACH ROW WHEN (pg_trigger_depth() = 0)
    EXECUTE PROCEDURE image_after_update();

CREATE OR REPLACE FUNCTION image_after_delete()
RETURNS TRIGGER AS $$
BEGIN
    UPDATE images SET sort = sort - 1 WHERE sort >= OLD.sort;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS image_after_delete_trigger ON images;
CREATE TRIGGER image_after_delete_trigger
    AFTER DELETE ON images
    FOR EACH ROW EXECUTE PROCEDURE image_after_delete();
