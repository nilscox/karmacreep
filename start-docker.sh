#!/bin/sh

docker run -it \
    -e PORT=80 \
    -e USER=foo \
    -e PASSWORD=bar \
    --link mypostgres:mypostgres \
    -p 4321:80 \
    karmacreep
