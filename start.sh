#!/bin/sh

for f in "index.js" "config.json"; do
    if [ ! -f "$f" ]; then
        echo "missing $f" 1>&2
        exit 1
    fi
done

for dir in "hd" "web" "thumb"; do
    if [ ! -d "public/images/$dir" ]; then
        echo "creating public/images/$dir"
        mkdir -p "public/images/$dir"
    fi
done

if [ -z "$PORT" ]; then
    echo "missing PORT environment variable" 1>&2
    exit 1
fi

if [ -z "$USER" ]; then
    echo "missing USER environment variable" 1>&2
    exit 1
fi

if [ -z "$PASSWORD" ]; then
    echo "missing PASSWORD environment variable" 1>&2
    exit 1
fi

echo "{\"$USER\": \"$PASSWORD\"}" > ./auth.json

echo "starting server"
node "./index.js"
