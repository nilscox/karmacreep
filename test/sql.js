const assert = require('assert');
const fs = require('fs');
const image = require('../api/routes');

const Image = image.Image;

describe('SQL', () => {

  before((done) => {
    image.sync()
      .then(() => done())
      .catch(done);
  });

  beforeEach((done) => {
    Image.truncate({ restartIdentity: true })
      .then(() => Image.bulkCreate([
        { filename: 'toto', sort: 1 },
        { filename: 'tata', sort: 2 },
        { filename: 'tutu', sort: 3 },
      ]))
      .then(() => done())
      .catch(done);
  });

  describe('insert', () => {

    it('should add a new image', (done) => {
      Image.create({ filename: 'tete', sort: 4 })
        .then(image => {
          assert.ok(image);
          assert.equal(image.id, 4);
          assert.equal(image.filename, 'tete');
          assert.equal(image.sort, 4);
          done();
        })
        .catch(done);
    });

  });

  describe('delete', () => {

    it('should delete the first image', (done) => {
      Image.findByPrimary(1)
        .then(image => {
          assert.ok(image);
          return image.destroy();
        })
        .then(() => Image.all({ order: [[ 'sort' ]] }))
        .then(images => {
          assert.ok(images);
          assert.equal(images.length, 2);
          assert.equal(images[0].id, 2);
          assert.equal(images[0].sort, 1);
          assert.equal(images[1].id, 3);
          assert.equal(images[1].sort, 2);
          done();
        })
        .catch(done);
    });

    it('should delete an image', (done) => {
      Image.findByPrimary(2)
        .then(image => {
          assert.ok(image);
          return image.destroy();
        })
        .then(() => Image.all({ order: [[ 'sort' ]] }))
        .then(images => {
          assert.ok(images);
          assert.ok(images.length, 2);
          assert.equal(images.length, 2);
          assert.equal(images[0].id, 1);
          assert.equal(images[0].sort, 1);
          assert.equal(images[1].id, 3);
          assert.equal(images[1].sort, 2);
          done();
        })
        .catch(done);
    });

    it('should delete the last image', (done) => {
      Image.findByPrimary(3)
        .then(image => {
          assert.ok(image);
          return image.destroy();
        })
        .then(() => Image.all({ order: [[ 'sort' ]] }))
        .then(images => {
          assert.ok(images);
          assert.equal(images.length, 2);
          assert.equal(images[0].id, 1);
          assert.equal(images[0].sort, 1);
          assert.equal(images[1].id, 2);
          assert.equal(images[1].sort, 2);
          done();
        })
        .catch(done);
    });

  });

  describe('update', () => {

    it('should fail to update the id field', (done) => {
      Image.findByPrimary(1)
        .then(image => {
          assert.ok(image);
          return image.update({ id: 42 });
        })
        .then(image => {
          assert.ok(image);
          assert.equal(image.id, 1);
          done();
        })
        .catch(done);
    });

    it('should update the filename field', (done) => {
      Image.findByPrimary(1)
        .then(image => {
          assert.ok(image);
          return image.update({ filename: 'tete' });
        })
        .then(image => {
          assert.ok(image);
          assert.equal(image.filename, 'tete');
          done();
        })
        .catch(done);
    });

    it('should update a duplicate filename field', (done) => {
      Image.findByPrimary(1)
        .then(image => {
          assert.ok(image);
          return image.update({ filename: 'tata' });
        })
        .then(() => done(new Error('field updated')))
        .catch(err => {
          assert.equal(err.message, 'Validation error');
          done();
        });
    });

    describe('sort same value', () => {

      it('should not update the first image', (done) => {
        Image.findByPrimary(1)
          .then(image => {
            assert.ok(image);
            return image.update({ sort: 1 });
          })
          .then(image => {
            assert.ok(image);
            assert.equal(image.sort, 1);
            return Image.all({order: [[ 'sort' ]]});
          })
          .then(images => {
            assert.ok(images);
            assert.equal(images.length, 3);
            images.forEach((image, i) => {
              assert.equal(image.id, i + 1);
              assert.equal(image.sort, i + 1);
            });
            done();
          })
          .catch(done);
      });

      it('should not update an image', (done) => {
        Image.findByPrimary(2)
          .then(image => {
            assert.ok(image);
            return image.update({ sort: 2 });
          })
          .then(image => {
            assert.ok(image);
            assert.equal(image.sort, 2);
            return Image.all({order: [[ 'sort' ]]});
          })
          .then(images => {
            assert.ok(images);
            assert.equal(images.length, 3);
            images.forEach((image, i) => {
              assert.equal(image.id, i + 1);
              assert.equal(image.sort, i + 1);
            });
            done();
          })
          .catch(done);
      });

      it('should not update the last image', (done) => {
        Image.findByPrimary(3)
          .then(image => {
            assert.ok(image);
            return image.update({ sort: 3 });
          })
          .then(image => {
            assert.ok(image);
            assert.equal(image.sort, 3);
            return Image.all({order: [[ 'sort' ]]});
          })
          .then(images => {
            assert.ok(images);
            assert.equal(images.length, 3);
            images.forEach((image, i) => {
              assert.equal(image.id, i + 1);
              assert.equal(image.sort, i + 1);
            });
            done();
          })
          .catch(done);
      });

    });

    describe('sort different values', () => {

      it('should update the first image to second place', (done) => {
        Image.findByPrimary(1)
          .then(image => {
            assert.ok(image);
            return image.update({ sort: 2 });
          })
          .then(image => {
            assert.ok(image);
            assert.equal(image.sort, 2);
            return Image.all({order: [[ 'sort' ]]});
          })
          .then(images => {
            assert.ok(images);
            assert.equal(images.length, 3);
            assert.equal(images[0].id, 2);
            assert.equal(images[0].sort, 1);
            assert.equal(images[1].id, 1);
            assert.equal(images[1].sort, 2);
            assert.equal(images[2].id, 3);
            assert.equal(images[2].sort, 3);
            done();
          })
          .catch(done);
      });


      it('should update an image to the first place', (done) => {
        Image.findByPrimary(2)
          .then(image => {
            assert.ok(image);
            return image.update({ sort: 1 });
          })
          .then(image => {
            assert.ok(image);
            assert.equal(image.sort, 1);
            return Image.all({order: [[ 'sort' ]]});
          })
          .then(images => {
            assert.ok(images);
            assert.equal(images.length, 3);
            assert.equal(images[0].id, 2);
            assert.equal(images[0].sort, 1);
            assert.equal(images[1].id, 1);
            assert.equal(images[1].sort, 2);
            assert.equal(images[2].id, 3);
            assert.equal(images[2].sort, 3);
            done();
          })
          .catch(done);
      });

      it('should update the last image to the first place', (done) => {
        Image.findByPrimary(3)
          .then(image => {
            assert.ok(image);
            return image.update({ sort: 1 });
          })
          .then(image => {
            assert.ok(image);
            assert.equal(image.sort, 1);
            return Image.all({order: [[ 'sort' ]]});
          })
          .then(images => {
            assert.ok(images);
            assert.equal(images.length, 3);
            assert.equal(images[0].id, 3);
            assert.equal(images[0].sort, 1);
            assert.equal(images[1].id, 1);
            assert.equal(images[1].sort, 2);
            assert.equal(images[2].id, 2);
            assert.equal(images[2].sort, 3);
            done();
          })
          .catch(done);
      });

      it('should apply a few updates that doesn\'t change the order', (done) => {
        Image.findByPrimary(1)
          .then(image => {
            assert.ok(image);
            return image.update({ sort: 3 });
          })
          .then(image => {
            assert.ok(image);
            assert.equal(image.sort, 3);
            return Image.findByPrimary(3);
          })
          .then(image => {
            assert.ok(image);
            return image.update({ sort: 2 });
          })
          .then(image => {
            assert.ok(image);
            assert.equal(image.sort, 2);
            return Image.findByPrimary(1);
          })
          .then(image => {
            assert.ok(image);
            return image.update({ sort: 2 });
          })
          .then(image => {
            assert.ok(image);
            assert.equal(image.sort, 2);
            return Image.findByPrimary(3);
          })
          .then(image => {
            assert.ok(image);
            return image.update({ sort: 3 });
          })
          .then(image => {
            assert.ok(image);
            assert.equal(image.sort, 3);
            return Image.all({order: [[ 'sort' ]]});
          })
          .then(images => {
            assert.ok(images);
            assert.equal(images.length, 3);
            assert.equal(images[0].id, 2);
            assert.equal(images[0].sort, 1);
            assert.equal(images[1].id, 1);
            assert.equal(images[1].sort, 2);
            assert.equal(images[2].id, 3);
            assert.equal(images[2].sort, 3);
            done();
          })
          .catch(done);
      });

    });

  });

  it('should mix adding, deleting and sorting images', (done) => {
    Image.create({ filename: 'tete', sort: 4 })
      .then(() => Image.findByPrimary(2))
      .then(image => image.update({ sort: 4 }))
      .then(() => Image.findByPrimary(3))
      .then(image => image.destroy())
      .then(() => Image.findByPrimary(4))
      .then(image => image.update({ sort: 1}))
      .then(() => Image.create({ filename: 'tutu', sort: 4 }))
      .then(() => Image.findByPrimary(1))
      .then(image => image.destroy())
      .then(() => Image.findByPrimary(5))
      .then(image => image.destroy())
      .then(() => Image.all({ order: [[ 'sort' ]] }))
      .then(images => {
        assert.ok(images);
        assert.equal(images.length, 2);
        assert.equal(images[0].id, 4);
        assert.equal(images[0].sort, 1);
        assert.equal(images[1].id, 2);
        assert.equal(images[1].sort, 2);
        done();
      })
      .catch(done);
  });

});