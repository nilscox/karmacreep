const path = require('path');

const config = require('./config');

let entries = {
  main: './react/main.js',
  admin: './react/admin.js'
};

let plugins = [];

const output = {
  path: path.resolve('public', 'js'),
  filename: '[name].js',
  publicPath: '/js/',
};

const loaders = [{
  test: /\.js$/,
  loaders: ['babel'],
  include: path.resolve('react')
}];

module.exports = {
  devtool: config.app.dev ? 'source-map' : false,
  entry: entries,
  output: output,
  plugins: plugins,
  module: {
    loaders: loaders,
  },
};